# TAID-DL.LC: Guía de integración BLE

## Servicios BLE

Además de los servicios estándar BLE para identificación, actualización de firmware, etc. , el dispositivo ofrece los siguientes servicios BLE para su operación. 

1. Un servicio para la gestión del registro de temperatura (se marcan con (**) las diferencias respecto a la versión prototipo)
```
// UUID del servicio de datalogging
var service_uuid_datalogger = '952545e0-33d9-47a2-91cb-59e689f30148';

// Características del servicio de datalogging
var char_uuids_datalogger = {
      // [readonly] último valor de temperatura medido (codificación según std Bluetooth SIG:
      //  https://www.bluetooth.com/wp-content/uploads/Sitecore-Media-Library/Gatt/Xml/Characteristics/org.bluetooth.characteristic.temperature.xml
      temperature: 0x2A6E,
      // [readwrite] Fecha/hora del equipo (codificación según std Bluetooth SIG:
      //  https://www.bluetooth.com/wp-content/uploads/Sitecore-Media-Library/Gatt/Xml/Characteristics/org.bluetooth.characteristic.date_time.xml )
      datetime: 0x2A08,
      // No implementado
      meas: 0x2A1C,

      //
      // La codificación de los siguientes valores multibyte es little endian (LSB-first)
      //

      // (**) [readonly] Máscara de dispositivos activos (2 bytes)
      activedevices: 'd07474a0-8ad4-454f-aa90-e8d3394e748c',

      // [readonly(**)] Periodo de muestreo, en segundos (3 bytes)
      period: '47e9f6c0-d45b-4e2b-bb4d-c536a170dac4',

      // [readonly] Tamaño de log disponible para lectura, en bytes (2 bytes)
      length: '9a6ea78f-077b-4016-86b3-b774dc34211e',

      // [readwrite] "Puntero" de escritura del log de datos (fijar a 0 para borrar el log) (4 bytes)
      writeptr: '19ade186-151c-45dd-9cce-d240f2555835',

      // [readwrite] "Puntero" de lectura del log de datos (fijar a 0 para iniciar la recogida de log) (4 bytes)
      readptr: 'd188f312-4ce8-4dcd-b9f3-97af0511ece6',

      // [readonly] Bloque de lectura de datos del log ([0,transfermtu] bytes)
      data: 'c38f3cca-e287-4fe2-9e71-d5e5be9fcfe9',

      // [readwrite] Tamaño en bytes del bloque de lectura de log (no modificar) (1 byte)
      transfermtu: 'fba28a46-3915-4947-9c64-d6e3b9dd63f9'

      // Para recoger el log:
      // 1. Leer "length"
      // 2. Fijar a 0 "readptr". 
      // 3. for (retrievedBytes=0, datalog=(); retrievedBytes < length; ) {
      //      datalogBlock = Leer "data" (la lectura de "data" avanza automáticamente readptr en transfermtu bytes
      //      retrievedBytes += datalogBlock.length
      //      datalog.append(datalogBlock)
      //    }
      // 4. Si se desea borrar el log tras la recogida, fijar a 0 "writeptr"
      // El "datalog" recogido tiene exactamente el mismo formato que el recogido vía RF
};
```

2. Un servicio para operaciones genéricas de identificación, reconfiguración y operación (no existente en la versión prototipo)
```
// UUID del servicio Taid Generic
var service_uuid_taid_generic = '0cd0d0cd-021e-4959-a208-6ad3bfa8ae95';

// Características del servicio Taid Generic
var char_uuids_taid_generic = {

  //
  // La codificación de los siguientes valores multibyte es little endian (LSB-first)
  //

  // [writeonly] Máscara de dispositivos que desencadena un muestreo de sensores (2 bytes)
  poll: '68f1bb8b-3f6f-4c04-ac13-776ed776a92a',
  // [readonly] Resultado del último muestreo de sensores (máx. 32 bytes)
  pollresult: '5d7a14b2-b1c2-4e0a-80c2-c23f8f2252ab',


  // [readonly] Identificación del equipo (máscara de dispositivos(2) + #serie(4) = 6 bytes)
  fulldeviceid: '645f17c0-59d9-432a-bfee-23b21c2ede04',

  // [writeonly] Parámetros que desencadenan la ejecución de un comando genérico (máx. 32 bytes)
  runcustomcommand: 'b96b1ff0-e07a-43e1-bf96-7f56bb59184e',
  // [readonly] Resultado de la ejecución del último comando genérico (máx. 32 bytes)
  runcustomcommandresult: '0f849777-e671-4dc3-b940-8b033d104e19',

  //
  // Las siguientes características permiten la edición del mapa de memoria del dispositivo
  //

  // [read] Lee memlength bytes de la dirección memadress (máx. 240 bytes)
  // [write] Escribe los bytes enviados a la característica en la dirección memadress (máx. 240 bytes)
  mem: '91af60e8-1972-438b-9520-e2bf0cc6ad43',
  // [readwrite] Puntero de lectura/escritura (4 bytes)
  memaddress: '59ffce20-4b6e-45e7-943d-67307dd11961',
  // [readwrite] Tamaño de las operaciones de lectura (1 byte)
  memlength: 'd94ac794-1eb1-4d33-9140-6261bbffb80c',
  // [readwrite] Configuración de opciones para la edición de memoria (4 bytes)
  // b0: autoincrementa memaddress en las operaciones de lectura
  // b1: autoincrementa memaddress en las operaciones de escritura
  memoptions: 'fd952ce1-8e82-4e8b-8b50-2aa59a7fab60'
};

```

## Interfaz de usuario: botón y led. Modos de operacion.

El led emite 3 parpadeos consecutivos, cada uno de los cuales puede ser “fuerte” o “débil”, cada T segundos:

  - El periodo T es (se aplica la primera condición que cumpla):
    - 2 segundos, si hay una conexión BLE establecida
    - 8 segundos, si la radio RF está en modo “online”
    - 16 segundos, si el advertising BLE está activo
    - 64 segundos

  - El primer parpadeo: es “débil” siempre, para permitir determinar si los siguientes son fuertes o débiles, por comparación

  - El segundo parpadeo: es “fuerte” si el datalogger está activo

  - El tercer parpadeo: es “fuerte” si hay alguna alarma activa

Una pulsación de la tecla puede desencadenar las siguientes acciones, según su duración y la configuración activa:

  - Corta: genera un evento de alarma, que realizará una acción u otra (datalogging, transmisión RF, etc), según la configuración

  - Larga (>= 3 segundos): genera un evento de activación de la/s radio/s (RF868 / BLE), según la configuración

  - Muy larga (>= 8 segundos): genera un evento de reset y recarga de la configuración por defecto opcionales

Mediante la variable de configuración `taid_opmode` se configura la activación, temporal o permanente, de ambas radios.

Las variables de configuración relacionadas son:
```
taid_opmode:
# b0: BLE activo tras reset del equipo
# b1: Pulsación corta reactiva BLE
# b2: BLE se desactiva por inactividad (60 segundos)
# b4: Se admiten sólo conexiones de dispositivos bonded (pulsación media abre ventana de aceptación) (requiere ble_passkey <> 0)
# b8: Subsistema RF activo tras reset del equipo (rf_autopoweroff_delay y rf_advert_period definen evolución posterior)
# b9: Pulsación corta mueve RF a estado online
# b14: Pulsación muy larga carga la configuración por defecto
# b15: Pulsación muy larga resetea el equipo

alrm_activedevices:
# b5: DEV_CARRIERDT (~botón)
# b15: FLAG_DLALARM
alrm_retxperiod:
# alrm_retxperiod: 0-1 única transmisión, 0xFFFF-sin transmisión RF (sólo DL_ALARM)

```

## Seguridad BLE

El dispositivo soporta 3 mecanismos de autenticación/encriptación, de menor a mayor seguridad:

  - Ninguno: no se requiere un procedimiento de emparejado (_bond_) con él. No proporciona encriptación de las comunicaciones.

  - _JustWorks_ pairing: requiere realizar un procedimiento de _bonding_ automático, que no requiere la introducción de password. Resulta en la encriptación de las comunicaciones, pero no ofrece protección MITM ("_Man-In-The-Middle_") durante el _bonding_.

  - _Passkey_ pairing: requiere realizar un procedimiento de _bonding_ utilizando una clave de 6 digitos conocida de antemano. Resulta en la encriptación de las comunicaciones y ofrece protección MITM.

Además el dispositivo permite ser configurado para aceptar sólo conexiones de equipos _bonded_ previamente. Esta opción habilita el botón para, mediante una pulsación media, abrir una ventana de aceptación de cualquier dispositivo, durante 60 segundos, en los que cualquier _peer_ BLE puede emparejarse con él.

Las variables de configuración relacionadas son:
```
taid_opmode:
# b4: Se admiten sólo conexiones de dispositivos bonded (pulsación media abre ventana de aceptación) (requiere ble_passkey <> 0)

ble_passkey:
# Configuración de seguridad:
#  0xFFFFFFFF, sin seguridad
#  == 0, con seguridad, Just Works pairing
#  <> 0, con seguridad, Passkey entry (password en DECIMAL) (en Android requiere completar con 0's a 6 dígitos)

```

*NOTA*: En Android la clave de emparejamiento debe rellenarse con ceros no significativos por la izquierda hasta completar 6 dígitos. Ejemplo
```
1234 -> 001234
```


## Mapa de memoria

La hoja de cálculo con la especificación completa del mapa de memoria puede descargarse [aquí](https://bitbucket.org/prodimar/activetag.dl-lc-integration/downloads/mapa_de_memoria_tag_activo_LC.xls).


## Acceso al mapa de memoria

Los métodos `Mem*` del servicio _Taid Generic_ permiten implementar el acceso de lectura y escritura al mapa de memoria del dispositivo.

Un acceso de lectura se realiza siguiendo los siguientes pasos:

1. Escribir la característica `memaddress` con la dirección de lectura del mapa de memoria deseada 

2. Escribir la característica `memlength` con el tamaño de datos a leer, respetando el máximo indicado

3. (Opcional) Escribir la característica `memoptions` con un valor con el bit 0 activado para que los accesos se realicen con autoincremento: de este modo, tras cada acceso de lectura, el valor de memaddress se incrementará automáticamente `(memaddress += memlength)`

4. Leer de la característica `mem` los `memlength` bytes

Correspondientemente, un acceso de escritura se realiza siguiendo los siguientes pasos:

1. Escribir la característica `memaddress` con la dirección de escritura del mapa de memoria deseada 

2. (Opcional) Escribir la característica `memoptions` con un valor con el bit 1 activado para que los accesos se realicen con autoincremento: de este modo, tras cada acceso de escritura, el valor de `memaddress` se incrementará automáticamente `(memaddress += número de bytes escritos a la característica mem)`

3. Escribir la característica `mem` con los datos deseados, respetando el máximo indicado

El siguiente ejemplo describe, en pseudocódigo, las operaciones necesarias para leer y reescribir la zona `USERDATA`:
```
USERDATA_ADDRESS = 0x1D00
USERDATA_LENGTH = 256
BLOCK_SIZE = 128

data = []
TaidGenericService.memaddress = USERDATA_ADDRESS
TaidGenericService.memlength = BLOCK_SIZE
TaidGenericService.memoptions = 0x03
for (offset = 0; offset < USERDATA_LENGTH; offset += BLOCK_SIZE) {
  data[offset .. offset + BLOCK_SIZE] = TaidGenericService.mem
}

// Hemos obtenido el contenido de la zona de memoria a data. La editamos convenientemente y ahora la reescribimos

TaidGenericService.memaddress = USERDATA_ADDRESS
for (offset = 0; offset < USERDATA_LENGTH; offset += BLOCK_SIZE) {
  TaidGenericService.mem = data[offset .. offset + BLOCK_SIZE] 
}

```

## Ejecución de comandos

Los métodos `runcustomcommand*` del servicio _Taid Generic_ permiten controlar la ejecución de un conjunto de comandos implementados por el dispositivo. Actualmente se definen los siguientes comandos de interés:

```
// El valor numérico es el índice de comando, en decimal:
//  1. Restore Default Config: Restaura la configuración de fábrica (no resetea el equipo)
// 22. Reset: Resetea el equipo
// 23. DeleteAllBLEBondings: Borra todos los bondings (dispositivos remotos vinculados con el equipo)
// 24. EnterConfigurationMode: Copia la configuración actual en un búffer de edición, e inicializa las características memaddress y memlength para que dicho buffer pueda ser leído
// 25. CommitConfiguration: Copia el contenido del búffer de edición como configuración actual

```

Para ejecutar un comando y obtener los datos obtenidos por el mismo deben seguirse los siguientes pasos:

1. Serializar en un búffer el índice del comando y los parámetros del mismo, de haberlos, según su especificación concreta (actualmente ninguno de los comandos de interés requiere parámetros)

2. Escribir el contenido de dicho búffer sobre la característica `runcustomcommand`. La escritura provocará la ejecución del comando y a partir de ese momento los datos resultado estarán accesibles como contenido de la característica `runcustomcommandresult`.

3. (Opcional) Si los datos resultado son de interés, obtenerlos mediante una lectura de la característica `runcustomcommandresult` y decodificarlos según la especificación concreta del comando (actualmente ninguno de los comandos devuelve datos resultado). Si un comando no devuelve datos resultado el resultado de la lectura de dicha característica tras su ejecución será de longitud 0.

El siguiente ejemplo ilustra, en pseudocódigo, la ejecución del comando para resetear el dispositivo:
```
buffer = [ 0x16 ] // Reset = 22d = 0x16
TaidGenericService.runcustomcommand = buffer

// No nos interesan los datos de respuesta del comando (que además sabemos que serían nulos), por tanto omitimos su lectura
// Tras un retardo de unos 5 segundos, el dispositivo se reiniciará
```


## Edición de la configuración

La manipulación de la configuración del equipo puede realizarse, conocido el mapa de memoria de configuración, mediante la utilización de las facilidades `mem*` y `runcustomcommand*`.

El siguiente ejemplo ilustra en pseudocódigo la modificación de la configuración del equipo para fijar un _ble_passkey_ con valor 123456 y permitir sólo la conexión de equipos _bonded_:

```
// Ejecutamos el comando EnterConfigurationMode
buffer = [ 0x18 ] // EnterConfigurationMode = 24d = 0x18
TaidGenericService.runcustomcommand = buffer

// El comando EnterConfigurationMode ya ha fijado memaddress y memlength con los parámetros del búffer de edición de la configuración
// Necesitamos salvar su dirección o tener desactivado el autoincremento en lectura en memoptions
cfg_edit_buffer_address = TaidGenericService.memaddress
cfg_edit_buffer_size = TaidGenericService.memlength // El tamaño de la configuración es 130 bytes, por lo que basta un único acceso de lectura/escritura para transferirla
cfg = TaidGenericService.mem

// Editamos cfg. Consultando el mapa de memoria sabemos lo siguiente:
// cfg = 01C3000040000060400003082C010001208000002C01005050500A2000200010001C20000005000010270A00200F0000043220000000000090E1CA0C1E...
//       ---- -> taid_opmode (LSB-first)                                              ble_passkey (LSB-first) <- --------
//
// Entonces activamos el bit 4 de taid_opmode y serializamos 123456 = 0x0001E240 LSB-first sobre ble_passkey, obteniendo:
// cfg = 11C3000040000060400003082C010001208000002C01005050500A2000200010001C20000005000010270A00200F00000432200040E2010090E1CA0C1E...
//       ---- -> taid_opmode (LSB-first)                                              ble_passkey (LSB-first) <- --------

// Escribimos completa la nueva cfg que hemos editado (fijamos previamente su dirección, por si el modo autoincremento estuviese activo)
TaidGenericService.memaddress = cfg_edit_buffer_address
TaidGenericService.mem = cfg

// Ejecutamos el comando CommitConfiguration
buffer = [ 0x19 ] // EnterConfigurationMode = 25d = 0x19
TaidGenericService.runcustomcommand = buffer

// El comando CommitConfiguration ya provoca el reset del equipo, por lo que tras un retardo de unos 5 segundos éste se reiniciará con la nueva configuración
```


## Lista de firmwares disponibles

* [versión 3.4/hw31](https://bitbucket.org/prodimar/activetag.dl-lc-integration/downloads/active_tag_dllc_20210615_3_4_0_hw31_mnf_21070002.gbl)
```
  Configuración:
    - Datalogger: periodo 5 min, dispositivos RTC+temperatura+batería
    - BLE: activo permanentemente, JustWorks pairing, se admite conexión de cualquier dispositivo
    - RF: activo permanentemente
```

* [versión 3.5/hw31](https://bitbucket.org/prodimar/activetag.dl-lc-integration/downloads/active_tag_dllc_20210623_3_5_0_hw31_mnf_21070002.gbl)
```
  Configuración:
    - Datalogger: desactivado
    - BLE: activo permanentemente, sin seguridad, se admite conexión de cualquier dispositivo
    - RF: activo permanentemente
```